@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
      <div class="col-3 p-5">
          <img src="https://scontent-frt3-1.cdninstagram.com/v/t51.2885-19/s150x150/23421188_341744572955988_281289386878828544_n.jpg?_nc_ht=scontent-frt3-1.cdninstagram.com&_nc_ohc=09EWYuvyDbUAX_sjZZI&oh=29bd98209f237df1538d78b8aaddca77&oe=5EB35523" class="rounded-circle">
      </div>
      <div class="col-9">
          <div><h1>el_torrada</h1></div>
          <div class="d-flex">
              <div class="pr-3"><strong>84</strong> posts</div>
              <div class="pr-3"><strong>698</strong> followers</div>
              <div class="pr-3"><strong>880</strong> following</div>
          </div>
          <div class="pt-4 font-weight-bold">
              Sérgio Marschall
          </div>
          <div>
              lorem
          </div>
          <div>
              <a href="#">www.google.com</a>
          </div>
      </div>
      <div class="row pt-5">
          <div class="col-4">
              <img class="w-100" src="https://scontent-frt3-2.cdninstagram.com/v/t51.2885-15/e35/c231.0.618.618a/84698311_731311917403537_6991574310735408360_n.jpg?_nc_ht=scontent-frt3-2.cdninstagram.com&_nc_cat=101&_nc_ohc=9kRXDbm1hesAX8DP54W&oh=5272e828da49671c8d765917bfa451b7&oe=5EB459AB">

          </div>
          <div class="col-4">
              <img class="w-100" src="https://scontent-frt3-2.cdninstagram.com/v/t51.2885-15/e35/c231.0.618.618a/84698311_731311917403537_6991574310735408360_n.jpg?_nc_ht=scontent-frt3-2.cdninstagram.com&_nc_cat=101&_nc_ohc=9kRXDbm1hesAX8DP54W&oh=5272e828da49671c8d765917bfa451b7&oe=5EB459AB">

          </div>
          <div class="col-4">
              <img class="w-100" src="https://scontent-frt3-2.cdninstagram.com/v/t51.2885-15/e35/c231.0.618.618a/84698311_731311917403537_6991574310735408360_n.jpg?_nc_ht=scontent-frt3-2.cdninstagram.com&_nc_cat=101&_nc_ohc=9kRXDbm1hesAX8DP54W&oh=5272e828da49671c8d765917bfa451b7&oe=5EB459AB">

          </div>
      </div>
  </div>
</div>
@endsection
